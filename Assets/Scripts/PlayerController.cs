﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    const int IPHONE_6_MAX_X = 750;
    const int IPHONE_6_MAX_Y = 1334;

    const int GAME_AREA_SCALE_X = 30;
    const int GAME_AREA_SCALE_Y = 60;

    const float IGNORE_TOUCH_DISTANCE = 0.5f;


    [SerializeField] private float speed = 30;
    [SerializeField] private float tilt = 1.1f;
    [SerializeField] private float xMin = -15;
    [SerializeField] private float xMax = 15;
    [SerializeField] private float yMin = -28;
    [SerializeField] private float yMax = 32;

    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }


    private void FixedUpdate()
    {
        float hAxis = Input.GetAxis("Horizontal");
        float vAxis = Input.GetAxis("Vertical");

        MoveTo(new Vector3(hAxis, vAxis, 0));

        if (Input.touchCount > 0)
        {
     
            Vector3 destination = -(rb.position - ScaleTouchDataToGameArea(Input.touches[0].position));

            if (destination.magnitude > IGNORE_TOUCH_DISTANCE)
            {
                MoveTo(destination.normalized);
            }
            

        }

    }

    private void MoveTo(Vector3 direction)
    {
        
        rb.velocity = direction * speed;

        float xPosition = Mathf.Clamp(rb.position.x, xMin, xMax);
        float yPosition = Mathf.Clamp(rb.position.y, yMin, yMax);

        rb.position = new Vector3(xPosition, yPosition, 0);

        rb.rotation = Quaternion.Euler(rb.velocity.y * tilt - 90, 0, -rb.velocity.x * tilt);
    }

    private Vector3 ScaleTouchDataToGameArea(Vector2 tocuh)
    {
        float x = GAME_AREA_SCALE_X * ((tocuh.x / IPHONE_6_MAX_X) - 0.5f);
        
        float y = GAME_AREA_SCALE_Y * ((tocuh.y / IPHONE_6_MAX_Y) - 0.5f);

        return new Vector3(x, y, 0);
    }


}
